package com.molyfun.bean.user;

import java.io.Serializable;


/**

 * 创建日期：2016-7-13

 * 作者：石冬生

 */
@SuppressWarnings("serial")
public class PhoneCaptchaBean implements Serializable{

	//验证码
    private String captchacode;
    //创建时间
    private Long createdatetime;
	/**
	 * @return the captchacode
	 */
	public String getCaptchacode() {
		return captchacode;
	}
	/**
	 * @param captchacode the captchacode to set
	 */
	public void setCaptchacode(String captchacode) {
		this.captchacode = captchacode;
	}
	/**
	 * @return the createdatetime
	 */
	public Long getCreatedatetime() {
		return createdatetime;
	}
	/**
	 * @param createdatetime the createdatetime to set
	 */
	public void setCreatedatetime(Long createdatetime) {
		this.createdatetime = createdatetime;
	}

}
