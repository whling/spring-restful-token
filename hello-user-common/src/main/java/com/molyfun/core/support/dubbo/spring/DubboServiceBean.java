package com.molyfun.core.support.dubbo.spring;


import com.alibaba.dubbo.config.spring.ServiceBean;
import com.molyfun.core.support.dubbo.spring.annotation.DubboService;

/**
 */
@SuppressWarnings("serial")
public class DubboServiceBean<T> extends ServiceBean<T> {
	public DubboServiceBean() {
		super();
	}

	public DubboServiceBean(DubboService service) {
		appendAnnotation(DubboService.class, service);
	}
}
