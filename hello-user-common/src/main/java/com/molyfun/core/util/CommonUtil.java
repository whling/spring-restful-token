package com.molyfun.core.util;


import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import com.molyfun.core.exception.BusinessException;

/**
 * 日期操作辅助类
 * 
 */
public final class CommonUtil {
	private CommonUtil() {
	}



	public static String captchaRandom(boolean numberFlag, int length){
		String retStr = "";
		String strTable = numberFlag ? "1234567890" : "1234567890abcdefghijkmnpqrstuvwxyz";
		int len = strTable.length();
		boolean bDone = true;
		do {
			retStr = "";
			int count = 0;
			for (int i = 0; i < length; i++) {
				double dblR = Math.random() * len;
				int intR = (int) Math.floor(dblR);
				char c = strTable.charAt(intR);
				if (('0' <= c) && (c <= '9')) {
					count++;
				}
				retStr += strTable.charAt(intR);
			}
			if (count >= 2) {
				bDone = false;
			}
		} while (bDone);
		return retStr;
	}

	//生成七位随机数，包含数字和字母
	public static String generateWord() {  
		String[] beforeShuffle = new String[] {"1", "2", "3", "4", "5", "6", "7",  
				"8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",  
				"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",  
				"W", "X", "Y", "Z" };  
		List list = Arrays.asList(beforeShuffle);  
		Collections.shuffle(list);  
		StringBuilder sb = new StringBuilder();  
		for (int i = 0; i < list.size(); i++) {  
			sb.append(list.get(i));  
		}  
		String afterShuffle = sb.toString();  
		String result = afterShuffle.substring(2, 9);  
		return result;  
	}


	//将bean中图片的路径由绝对路径换为相对路径
	public static void checkBeanImgpath(Object obj)
	{
		PropertyDescriptor origDescriptors[] = PropertyUtils
				.getPropertyDescriptors(obj);
		String  name, type;
		for (int i = 0; i < origDescriptors.length; i++)
		{
			name = origDescriptors[i].getName();
			type = origDescriptors[i].getPropertyType().toString();
			try {
				if ("interface java.util.List".equals(type)) {
					Object value = PropertyUtils.getSimpleProperty(obj, name);
					if(value == null)
					{
						continue;
					}
					List<Object> olist = (List)value;
					for(Object o : olist)
					{
						checkBeanImgpath(o);
					}
				}	
				else if ("class java.lang.String".equals(type))
				{
					if(("headimgurl".equals(name) || "logo".equals(name) || "url".equals(name)))
					{
						Object value = PropertyUtils.getSimpleProperty(obj, name);
						if(value == null)
						{
							continue;
						}
						String url = WebUtil.imagesRelativePath((String)value);
						PropertyUtils.setProperty(obj, name, url);
					}
				}
			} catch (Exception e) {
				throw new BusinessException("服务出错");
			} 
		}
	}


	//将bean中属性中的中文进行encode
	public static void BeanParamAssembly(Object obj)
	{

		PropertyDescriptor origDescriptors[] = PropertyUtils
				.getPropertyDescriptors(obj);
		String  name, type;
		for (int i = 0; i < origDescriptors.length; i++)
		{
			name = origDescriptors[i].getName();
			type = origDescriptors[i].getPropertyType().toString();
			try 
			{
				if ("class java.lang.String".equals(type))
				{
					if(("headimgurl".equals(name) || "logo".equals(name) || "url".equals(name)) )
					{
						Object value  = PropertyUtils.getSimpleProperty(obj, name);
						if(value == null  )
						{
							continue;
						}
						String deVal  = value.toString();
						String newVal = "";
						newVal = deVal == null || "".equals(deVal) || "undefined".equals(deVal) ? "" :"www.baidu.com/" +deVal;
						PropertyUtils.setProperty(obj, name, newVal);
					}

				}
			} catch (Exception e) {
				throw new BusinessException("服务出错");
			}
		}
	}
}
