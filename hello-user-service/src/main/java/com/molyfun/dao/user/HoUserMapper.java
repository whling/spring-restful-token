package com.molyfun.dao.user;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.molyfun.core.base.BaseMapper;
import com.molyfun.model.user.HoUser;

/**
 * 由MyBatis Generator工具自动生成，请不要手动修改
 */
public interface HoUserMapper extends BaseMapper<HoUser> {

	/**
	 * @param phone
	 * @return
	 */
	List<HoUser> selectByPhone(@Param("phone") String phone);

	/**
	 * @param username
	 * @return
	 */
	List<HoUser> selectByUserNameAndPhone(@Param("username") String userName,@Param("password") String password);

	/**
	 * @param name
	 * @return
	 */
	List<HoUser> selectByName(@Param("username") String name);

	/**
	 * @param token
	 * @return
	 */
	String selectUserIdByToken(@Param("token") String token);
}