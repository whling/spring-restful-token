/**
 * 
 */
package com.molyfun.dao.sys;

import org.apache.ibatis.annotations.Param;

import com.molyfun.core.base.BaseExpandMapper;

/**
 * @author ShenHuaJie
 */
public interface SysUserExpandMapper extends BaseExpandMapper {

	String queryUserIdByThirdParty(@Param("provider") String provider, @Param("openId") String openId);
}
