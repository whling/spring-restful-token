package com.molyfun.dao.student;

import com.molyfun.core.base.BaseMapper;
import com.molyfun.model.student.MhStudentActions;

/**
 * 由MyBatis Generator工具自动生成，请不要手动修改
 */
public interface MhStudentActionsMapper extends BaseMapper<MhStudentActions> {
}