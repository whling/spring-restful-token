package com.molyfun.service.student;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.molyfun.bean.student.MhStudentBean;
import com.molyfun.core.Constants;
import com.molyfun.core.base.BaseService;
import com.molyfun.core.support.Assert;
import com.molyfun.core.support.mq.QueueSender;
import com.molyfun.core.util.PasswordUtil;
import com.molyfun.model.student.MhStudent;
import com.molyfun.provider.student.MhStudentProvider;

@Service
public class MhStudentServiceImpl extends BaseService<MhStudentProvider, MhStudent>{
   @Autowired
    public void setProvider(MhStudentProvider provider){
	   this.provider=provider; 
   }
   
    private QueueSender queueSender;
	// 线程池
	private ExecutorService executorService = Executors.newCachedThreadPool();

  /*  //获取邀请码
	@Override
	public String inviteCode(String cardid) {
		// TODO Auto-generated method stub
		Assert.idCard(cardid);
		String key = getInviteKey(cardid);
		return key;
	}*/


	/** 生成学生邀请码 */
	
	public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
        "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
        "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
        "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
        "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
        "W", "X", "Y", "Z" };

	public static String getInviteKey(String cardid) {
	    StringBuffer shortBuffer = new StringBuffer();
	    String uuid = UUID.randomUUID().toString().replace("-", "");
	    for (int i = 0; i < 8; i++) {
	        String str = uuid.substring(i * 4, i * 4 + 4);
	        int x = Integer.parseInt(str, 16);
	        shortBuffer.append(chars[x % 0x3E]);
	    }
	    shortBuffer.append(cardid);
	    return shortBuffer.toString();
	}
	
	
	
	
	
	
	//获取学生邀请码
	public String findInviteCode(String cardId){
			Assert.idCard(cardId);
		    String inviteCode=provider.findInviteCode(cardId);
			return inviteCode;
	}
	
    //邀请码关联学生
	public void saveInviteCode(String cardId,MhStudentBean msb) {
		    Assert.idCard(cardId);
		    MhStudent stu=provider.queryByCardId(cardId);
		 	stu.setInviteCode(msb.getInviteCode());
		 	provider.update(stu);
		 		
	}
	
	//查询学生信息
	public MhStudent findStudentInfo(String cardId){
		  Assert.idCard(cardId);
		  MhStudent stuInfo=provider.queryByCardId(cardId);
		  return stuInfo;
	}


	
}
