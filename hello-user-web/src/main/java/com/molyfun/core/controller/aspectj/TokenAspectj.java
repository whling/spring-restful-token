package com.molyfun.core.controller.aspectj;  

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.Aspect;  
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;  
import org.springframework.ui.ModelMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.StringUtil;
import com.molyfun.core.exception.BusinessException;
import com.molyfun.core.support.HttpCode;
import com.molyfun.core.util.WebUtil;
import com.molyfun.model.user.HoUser;

@Component
@Aspect  
public class TokenAspectj {  

	@Pointcut("execution(public * com.molyfun.web..*.*(..))")
	public void checkToken(){}


	@Before("checkToken()")
	public void before() {
//		HttpServletRequest request = WebUtil.getRequest();
//		if(request.getAttribute("isCheckToken") != null)
//		{
//			String token = (String) request.getAttribute("token");
//			String userId = this.userService.queryUserIdByToken(token);
//			if(StringUtil.isEmpty(userId))
//			{
//				//验证token
////				ModelMap modelMap = new ModelMap();
////				modelMap.put("code", HttpCode.BUSINESS_FAIL.value());
////				modelMap.put("msg", "token验证失败,请重新登陆");
////				response.setContentType("application/json;charset=UTF-8");
////				byte[] bytes = JSON.toJSONBytes(modelMap, SerializerFeature.DisableCircularReferenceDetect);
////				response.getOutputStream().write(bytes);
//			}
//			request.setAttribute("userId", userId);
////			HoUser user = this.userService.queryById(userId);
////			UserThreadLocal.set(user);
//		}
	}
}